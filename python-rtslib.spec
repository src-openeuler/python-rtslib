%bcond_without python3

%global oname rtslib-fb

Name: python-rtslib
Version: 2.1.76
Release: 2
Summary: Python object API for Linux kernel LIO SCSI target

License: ASL 2.0
URL: https://github.com/open-iscsi/%{oname}
Source0: %{url}/archive/v%{version}/%{oname}-%{version}.tar.gz

BuildArch: noarch
BuildRequires: systemd-units
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Provides: python-rtslib-doc
Obsoletes: python-rtslib-doc

%description
%{name} is a python object API for generic Linux SCSI kernel target
which includes the 'target' service and targetctl tool for restoring
configuration.

%if %{with python3}
%package -n python3-rtslib
Summary: Python3 object API for Linux kernel LIO SCSI target
BuildRequires: python3-devel, python3-setuptools
Requires: python3-kmod, python3-six, python3-pyudev
%{?python_provide:%python_provide python3-rtslib}
provides: python3-%{oname}

%description -n python3-rtslib
python3-rtslib is a python object API for generic Linux SCSI kernel
target which includes the 'target' service and targetctl tool for
restoring configuration.
%endif

%package -n target-restore
Summary: Systemd service for targetcli/rtslib
%if %{with python3}
Requires: python3-rtslib = %{version}-%{release}
%endif
 
%description -n target-restore
Systemd service to restore the LIO kernel target settings on system
restart.

%package_help

%prep
%autosetup -n %{oname}-%{version} -p1

%if %{with python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
gzip --stdout doc/targetctl.8 > doc/targetctl.8.gz
gzip --stdout doc/saveconfig.json.5 > doc/saveconfig.json.5.gz

%if 0%{?with_python3}
pushd %{py3dir}
%py3_build
popd
%endif

%install
%if %{with python3}
pushd %{py3dir}
%py3_install
popd
%endif

mkdir -p %{buildroot}%{_mandir}/man8/
mkdir -p %{buildroot}%{_mandir}/man5/
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/target/backup
mkdir -p %{buildroot}%{_localstatedir}/target/pr
mkdir -p %{buildroot}%{_localstatedir}/target/alua
mkdir -p %{buildroot}%{_bindir}
install -m 644 systemd/target.service %{buildroot}%{_unitdir}/target.service
install -m 644 doc/targetctl.8.gz %{buildroot}%{_mandir}/man8/
install -m 644 doc/saveconfig.json.5.gz %{buildroot}%{_mandir}/man5/
install -m 755 scripts/targetctl %{buildroot}%{_bindir}/

%post -n target-restore
%systemd_post target.service

%preun -n target-restore
%systemd_preun target.service

%postun -n target-restore
%systemd_postun_with_restart target.service

%if %{with python3}
%files -n python3-rtslib
%defattr(-,root,root)
%license COPYING
%{python3_sitelib}/*
%endif

%files -n target-restore
%defattr(-,root,root)
%{_bindir}/targetctl
%{_unitdir}/target.service
%dir %{_sysconfdir}/target
%dir %{_sysconfdir}/target/backup
%dir %{_localstatedir}/target
%dir %{_localstatedir}/target/pr
%dir %{_localstatedir}/target/alua

%files help
%defattr(-,root,root)
%doc README.md doc/getting_started.md
%{_mandir}/man8/targetctl.8.gz
%{_mandir}/man5/saveconfig.json.5.gz

%changelog
* Tue Apr 16 2024 Pengda Dou <doupengda@loongson.cn> - 2.1.76-2
- Fix missing targetctl file error

* Wed Jan 10 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 2.1.76-1
- Update package to version 2.1.76

* Fri Oct 28 2022 Wenchao Hao <haowenchao@huawei.com> - 2.1.75-1
- update to rtslib-fb-2.1.75

* Thu Aug 11 2022 huangtianhua <huangtianhua@huawei.com> - 2.1.74-2
- Provides python3-rtslib-fb

* Tue Nov 16 2021 Wenchao Hao <haowenchao@huawei.com> - 2.1.74-1
- update to rtslib-fb-2.1.74

* Mon Nov 2 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 2.1.70-7
- backport upstream patch to solve one problem

* Fri Oct 30 2020 tianwei <tianwei12@huawei.com> - 2.1.70-6
- remove python2

* Fri Jul 17 2020 Ruijun Ge <geruijun@huawei.com> - 2.1.70-5
- fix CVE-2020-14019

* Sun Jul 5 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 2.1.70-4
- remove useless readme files.

* Thu Jan 09 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.1.70-3
- re-package for rtslib-fb

* Wed Jan 01 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.1.70-2
- Strenthen spec

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.1.70-1
- Package init
